cd ../src

cat qd.Common.js \
  math/qd.math.js math/qd.Point2D.js math/qd.Vector2D.js math/qd.Vector3D.js math/qd.Angle.js \
  collections/*.js \
  physics/qd.Body.js physics/qd.Collision.js physics/qd.Physics.js physics/qd.BoundingBox.js physics/qd.Cloth.js physics/qd.DynamicSpring.js physics/qd.Kinematics.js physics/qd.Spring.js physics/qd.CollisionBox.js \
  graphics/*.js \
  animation/*.js \
  io/*.js \
  qd.Element.js \
  qd.Entity.js \
  qd.EntitySet.js \
  qd.Layer.js \
  qd.World.js \
  qd.Engine.js \
  editor/*.js \
  > ../build/qd.js

cd ../build

clear
echo 'Successfully built qd.js. Go check if it is still working, you hack.'
