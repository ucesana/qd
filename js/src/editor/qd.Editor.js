(function (qd) {

    /**
     * Quick & Dirty Editor
     */
    qd.Editor = function (engine) {

        /* Public */

        this.newCanvas = function () {
            _engine.pause(function () {
                qd.Canvas.SET_TO_DEFAULT_STYLES(_styles);
                _selected.clear();
                _clipboard.clear();
                _world.clear();
                _handleSet.reset();
                _camera.reset();
                _grid.reset();
                _switchMode(_lineMode);
                _canvas.clear();
                _physics.clear();
                qd.Entity.ID_GEN.reset();
                _engine.run();
            });

            return this;
        };

        this.saveCanvas = function () {
            _mode.deactivate();
            _engine.pause(function () {
                _canvas.clear();
                _editor.draw();
                qd.download("Created By Quick and Dirty", _canvas.toImage(_mimeType), _mimeType);
                _mode.activate();
                _engine.run();
            });
            return this;
        };

        this.import = function () {
            var files = qd.getElementById(_commands.fileImport.id).files;

            if (qd.isNotEmpty(files)) {
                qd.forEach(files, function (file) {
                    _editor.importImage(file);
                });
            }
            return this;
        };

        this.importImage = function (file) {
            var reader = new FileReader(),
                src;

            reader.addEventListener("load", function () {
                src = reader.result;

                _view.sprite(src).load(function (sprite) {
                    var spritePos = qd.math.scatter(_view.centre(), _view.width() / 2);
                    sprite.position(spritePos[0], spritePos[1]);

                    if (sprite.width() > _view.width()) {
                        sprite.scaleToWidth(_view.width() / 2);
                    }
                    _addNewEntity(sprite);
                });
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
            return this;
        };

        this.dropImage = function (event) {
            //noinspection JSUnresolvedVariable
            var dataTransfer = event.dataTransfer,
                files = dataTransfer.files;

            if (qd.isNotEmpty(files)) {
                qd.forEach(files, function (file) {
                    this.importImage(file);
                }, this);
            }
        };

        this.previewCanvas = function () {
            _mode.deactivate();

            _engine.pause(function () {
                _canvas.clear();
                _editor.draw();
                window.open(_canvas.toImage(_mimeType));
                _mode.activate();
                _engine.run();
            });

            return this;
        };

        this.cut = function () {
            _clipboard.clear();

            _selected.each(function (entity) {
                entity.undraggable().unselectable();
                _clipboard.add(entity);
                _world.removeEntity(entity);
            });

            _selected.clear();

            return this;
        };

        this.copy = function () {
            _clipboard = _selected.clone();

            return this;
        };

        this.paste = function () {
            var copy = _clipboard.clone(),
                centroid = copy.centroid(),
                scatterPnt = qd.math.scatter(_view.centre(), _view.width() / 2);

            _selected.clear();

            copy.each(function (entity) {
                entity.translate(scatterPnt[0] - centroid[0], scatterPnt[1] - centroid[1]);
                _selected.add(entity);
                _world.addEntity(entity.selectable().draggable());
            });

            return this;
        };

        this.delete = function () {
            _selected.each(function (entity) {
                _world.destroyEntity(entity);
            });

            _selected.clear();

            return this;
        };

        this.moveLeft = function () {
            if (_selected.empty()) {
                _camera.translate(-_moveStep(), 0);
            } else {
                _selected.translate(-_moveStep(), 0)
            }
            return this;
        };

        this.moveRight = function () {
            if (_selected.empty()) {
                _camera.translate(_moveStep(), 0);
            } else {
                _selected.translate(_moveStep(), 0);
            }
            return this;
        };

        this.moveUp = function () {
            if (_selected.empty()) {
                _camera.translate(0, -_moveStep());
            } else {
                _selected.translate(0, -_moveStep());
            }
            return this;
        };

        this.moveDown = function () {
            if (_selected.empty()) {
                _camera.translate(0, _moveStep());
            } else {
                _selected.translate(0, _moveStep());
            }
            return this;
        };

        this.zoomIn = function () {
            _camera.zoomIn();
            return this;
        };

        this.zoomOut = function () {
            _camera.zoomOut();
            return this;
        };

        this.rotateClockwise = function () {
            _selected.rotate(qd.math.toRadians(_DEGREE_STEP));
            return this;
        };

        this.rotateAnticlockwise = function () {
            _selected.rotate(qd.math.toRadians(-_DEGREE_STEP));
            return this;
        };

        this.increaseScale = function () {
            _selected.scale(_SCALE_STEP_INC, _SCALE_STEP_INC);
            return this;
        };

        this.decreaseScale = function () {
            _selected.scale(_SCALE_STEP_DEC, _SCALE_STEP_DEC);
            return this;
        };

        this.scaleLeft = function () {
            _selected.scale(_SCALE_STEP_DEC, 1);
            return this;
        };

        this.scaleRight = function () {
            _selected.scale(_SCALE_STEP_INC, 1);
            return this;
        };

        this.scaleUp = function () {
            _selected.scale(1, _SCALE_STEP_INC);
            return this;
        };

        this.scaleDown = function () {
            _selected.scale(1, _SCALE_STEP_DEC);
            return this;
        };

        this.skewLeft = function () {
            _selected.skew(-_SKEW_STEP, 0);
            return this;
        };

        this.skewRight = function () {
            _selected.skew(_SKEW_STEP, 0);
            return this;
        };

        this.skewUp = function () {
            _selected.skew(0, -_SKEW_STEP);
            return this;
        };

        this.skewDown = function () {
            _selected.skew(0, _SKEW_STEP);
            return this;
        };

        this.selectMode = function () {
            return _switchMode(_selectMode);
        };

        this.nodeMode = function () {
            return _switchMode(_nodeMode)
        };

        this.lineMode = function () {
            return _switchMode(_lineMode);
        };

        this.circleMode = function () {
            return _switchMode(_circleMode);
        };

        this.polygonMode = function () {
            return _switchMode(_polygonMode);
        };

        this.polylineMode = function () {
            return _switchMode(_polylineMode);
        };

        this.penMode = function () {
            return _switchMode(_penMode);
        };

        this.quadraticCurveMode = function () {
            return _switchMode(_quadraticCurveMode);
        };

        this.closedQuadraticCurveMode = function () {
            return _switchMode(_closedQuadraticCurveMode);
        };

        this.physicsMode = function () {
            return _switchMode(_physicsMode);
        };

        this.smoothPath = function() {
            _selected.eachGraphic(function (graphic) {
                graphic.shape().halve();
            });
            return this;
        };

        this.unsmoothPath = function() {
            _selected.eachGraphic(function (graphic) {
                graphic.shape().double();
            });
            return this;
        };

        this.textMode = function () {
            return _switchMode(_textMode);
        };

        this.cancelMode = function () {
            _mode.cancel();
        };

        this.stroke = function () {
            return _applyStyle(_commands.colourStroke);
        };

        this.fill = function () {
            return _applyStyle(_commands.colourFill);
        };

        this.fillRuleEvenOdd = function () {
            return _applyStyle(_commands.colourFillRuleEvenOdd);
        };

        this.fillRuleNonZero = function () {
            return _applyStyle(_commands.colourFillRuleNonZero);
        };

        this.lineWidth = function () {
            return _applyStyle(_commands.lineWidth);
        };

        this.lineDash = function () {
            return _applyStyle(_commands.lineDash);
        };

        this.lineDashOffset = function () {
            return _applyStyle(_commands.lineDashOffset);
        };

        this.lineMiterLimit = function () {
            return _applyStyle(_commands.lineMiterLimit);
        };

        this.lineMiterLimit = function () {
            return _applyStyle(_commands.lineMiterLimit);
        };

        this.lineCapButt = function () {
            return _applyStyle(_commands.lineCapButt);
        };

        this.lineCapRound = function () {
            return _applyStyle(_commands.lineCapRound);
        };

        this.lineCapSquare = function () {
            return _applyStyle(_commands.lineCapSquare);
        };

        this.lineJoinBevel = function () {
            return _applyStyle(_commands.lineJoinBevel);
        };

        this.lineJoinRound = function () {
            return _applyStyle(_commands.lineJoinRound);
        };

        this.lineJoinMiter = function () {
            return _applyStyle(_commands.lineJoinMiter);
        };

        this.writeText = function () {
            var fontText = qd.Element.getById(_commands.textWriter.id);
            _writingPad.write(fontText.value());
        };

        this.fontFamily = function () {
            return _applyStyle(_commands.fontFamily);
        };

        this.fontSize = function () {
            return _applyStyle(_commands.fontSize);
        };

        this.textAlignLeft = function () {
            return _applyStyle(_commands.textAlignLeft);
        };

        this.textAlignCentre = function () {
            return _applyStyle(_commands.textAlignCentre);
        };

        this.textAlignRight = function () {
            return _applyStyle(_commands.textAlignRight);
        };

        this.textBaselineTop = function () {
            return _applyStyle(_commands.textBaselineTop);
        };

        this.textBaselineHanging = function () {
            return _applyStyle(_commands.textBaselineHanging);
        };

        this.textBaselineMiddle = function () {
            return _applyStyle(_commands.textBaselineMiddle);
        };

        this.textBaselineAlphabetic = function () {
            return _applyStyle(_commands.textBaselineAlphabetic);
        };

        this.textBaselineIdeographic = function () {
            return _applyStyle(_commands.textBaselineIdeographic);
        };

        this.textBaselineBottom = function () {
            return _applyStyle(_commands.textBaselineBottom);
        };

        this.textDirectionLTR = function () {
            return _applyStyle(_commands.textDirectionLTR);
        };

        this.textDirectionRTL = function () {
            return _applyStyle(_commands.textDirectionRTL);
        };

        this.shadowBlur = function () {
            return _applyStyle(_commands.shadowBlur);
        };

        this.shadowColour = function () {
            return _applyStyle(_commands.shadowColour);
        };

        this.shadowOffsetX = function () {
            return _applyStyle(_commands.shadowOffsetX);
        };

        this.shadowOffsetY = function () {
            return _applyStyle(_commands.shadowOffsetY);
        };

        this.groupSelection = function () {
            var group = _world.createEntity();

            _selected.sortZOrder();

            _selected.each(function (entity) {
                qd.forEach(entity.graphics(), function (graphic) {
                    group.graphic(graphic.clone());
                });
            });

            _selected.each(function (entity) {
                _world.destroyEntity(entity);
            });

            _selected.clear();

            _selected.add(group.selectable().draggable());
        };

        this.ungroupSelection = function () {
            var graphics = [];

            _selected.each(function (entity) {
                qd.forEach(entity.graphics(), function (graphic) {
                    graphics.push(graphic.clone());
                });

                _world.destroyEntity(entity);
            });

            _selected.clear();

            qd.forEach(graphics, function (graphic) {
                _selected.add(_world.createEntity()
                    .graphic(graphic)
                    .selectable()
                    .draggable());
            });

        };

        this.raiseSelection = function () {
            _selected.raise();
        };

        this.raiseSelectionToTop = function () {
            _selected.raiseToTop();
        };

        this.lowerSelection = function () {
            _selected.lower();
        };

        this.lowerSelectionToBottom = function () {
            _selected.lowerToBottom();
        };

        this.linearDamping = function () {
            _applyPhysicsConstant(_commands.physicsDampingLinear)
        };

        this.rotationalDamping = function () {
            _applyPhysicsConstant(_commands.physicsDampingRotational)
        };

        this.gravity = function () {
            _applyPhysicsConstant(_commands.physicsGravity)
        };

        this.gravitationalConstant = function () {
            _applyPhysicsConstant(_commands.physicsGravitationalConstant)
        };

        this.bodyActive = function () {
            _applyBodyProperty(_commands.bodyActive);
        };

        this.bodyDensity = function () {
            _applyBodyProperty(_commands.bodyDensity);
        };

        this.bodyMoment = function () {
            _applyBodyProperty(_commands.bodyMoment);
        };

        this.restitution = function () {
            _applyBodyProperty(_commands.bodyRestitution)
        };

        this.toggleToolbar = function () {
            _contextualToolbar.toggle();
            _editor.resizeToClientWindow();
        };

        this.toggleGrid = function () {
            _grid.toggle();
        };

        this.resizeToClientWindow = function () {
            var winMetrics = qd.measureClientWindow(),
                width = winMetrics.width,
                height = winMetrics.height,
                resizables;

            if (_contextualToolbar.visible()) {
                height = height - _contextualToolbar.height();
            }

            resizables = qd.Element.find(".qd-resizable");

            qd.forEach(resizables, function (resizable) {
                resizable.attr("width", width).attr("height", height);
            });

            _view.resize(width, height);
            _grid.draw();
        };

        this.step = function (t, dt) {
            _world.step(t, dt);
        };

        this.draw = function () {
            _canvas.clear();
            _world.draw(_canvas);
            _mode.draw(_canvas);
            return this;
        };

        /** Private */

        var _MOVE_STEP = 12,
            _SCALE_STEP_INC = 1.02,
            _SCALE_STEP_DEC = 0.98,
            _DEGREE_STEP = 1,
            _SKEW_STEP = 0.05,
            _DASHED = { "lineWidth": 1.0, "lineDash": [4, 4], "stroke": qd.Q_BLUE },

            _editor = this,

            _engine = engine,
            _canvas = engine.canvas(),
            _view = engine.view(),
            _camera = engine.camera(),
            _world = engine.world(),
            _physics = engine.physics(),

            _grid = new qd.Grid("grid", _camera),

            _mouse = engine.mouse(),
            _keyboard = engine.keyboard(),

            _mimeType = "image/png",

            _selected = new qd.EntitySet(),
            _clipboard = new qd.EntitySet(),
            _handleSet = new qd.HandleSet(),
            _styles = qd.Canvas.CREATE_DEFAULT_STYLES(),

            _WritingPad = function () {
                this.write = function (text) {
                    this._pen.call(this._paper, text);
                };

                this.writer = function (paper, pen) {
                    this._paper = paper;
                    this._pen = pen;
                };
            },

            _writingPad = new _WritingPad(),
            
            _commands = {
                fileNew: {
                    id: "fileNew",
                    panel: "file",
                    type: "button",
                    label: "New",
                    shortcut: "alt+shift+n",
                    action: _editor.newCanvas
                },
                fileSave: {
                    id: "file-save",
                    panel: "file",
                    type: "button",
                    label: "Save",
                    shortcut: "alt+shift+s",
                    action: _editor.saveCanvas
                },
                fileImport: {
                    id: "file-import",
                    panel: "file",
                    type: "file",
                    label: "Import",
                    shortcut: "alt+shift+i",
                    shortcutHandler: function () {
                        window.document.getElementById(_commands.fileImport.id).click();
                    },
                    action: _editor.import
                },
                filePreview: {
                    id: "file-preview",
                    panel: "file",
                    type: "button",
                    label: "Preview",
                    shortcut: "alt+shift+p",
                    action: _editor.previewCanvas
                },
                viewZoomIn: {
                    id: "view-zoom-in",
                    panel: "view",
                    type: "button",
                    label: "Zoom In",
                    icon: "+",
                    shortcut: "z",
                    action: _editor.zoomIn
                },
                viewZoomOut: {
                    id: "view-zoom-out",
                    panel: "view",
                    type: "button",
                    label: "Zoom Out",
                    icon: "-",
                    shortcut: "shift+z",
                    action: _editor.zoomOut,
                    divider: "bar"
                },
                viewFullscreen: {
                    id: "view-full-screen",
                    panel: "view",
                    type: "button",
                    label: "Full Screen",
                    shortcut: "alt+shift+f",
                    action: qd.toggleFullScreen,
                    divider: "bar"
                },
                viewMoveUp: {
                    id: "view-move-up",
                    panel: "view",
                    type: "button",
                    label: "Move Up",
                    icon: "↑",
                    shortcut: "w/up",
                    action: _editor.moveUp
                },
                viewMoveLeft: {
                    id: "view-move-left",
                    panel: "view",
                    type: "button",
                    label: "Move Left",
                    icon: "←",
                    shortcut: "a/left",
                    action: _editor.moveLeft
                },
                viewMoveRight: {
                    id: "view-move-right",
                    panel: "view",
                    type: "button",
                    label: "Move Right",
                    icon: "→",
                    shortcut: "d/right",
                    action: _editor.moveRight
                },
                viewMoveDown: {
                    id: "view-move-down",
                    panel: "view",
                    type: "button",
                    label: "Move Down",
                    icon: "↓",
                    shortcut: "s/down",
                    action: _editor.moveDown
                },
                editCut: {
                    id: "edit-cut",
                    panel: "edit",
                    type: "button",
                    label: "Cut",
                    icon: "✂",
                    shortcut: "ctrl+x/⌘+x",
                    action: _editor.cut
                },
                editCopy: {
                    id: "edit-copy",
                    panel: "edit",
                    type: "button",
                    label: "Copy",
                    icon: "⧉",
                    shortcut: "ctrl+c/⌘+c",
                    action: _editor.copy
                },
                editPaste: {
                    id: "edit-paste",
                    panel: "edit",
                    type: "button",
                    label: "Paste",
                    icon: "▣",
                    shortcut: "ctrl+v/⌘+v",
                    action: _editor.paste
                },
                editDelete: {
                    id: "edit-delete",
                    panel: "edit",
                    type: "button",
                    label: "Delete",
                    icon: "☓",
                    shortcut: "ctrl+d/⌫",
                    action: _editor.delete
                },
                modePen: {
                    id: "mode-pen",
                    panel: "mode",
                    type: "button",
                    label: "Pen",
                    icon: "Pen",
                    shortcut: "p",
                    action: _editor.penMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modeSelect: {
                    id: "mode-select",
                    panel: "mode",
                    type: "button",
                    label: "Select",
                    shortcut: "e",
                    action: _editor.selectMode,
                    tools: ["edit", "transform", "colour", "fill-rule", "object", "line-style", "line-join",
                        "line-cap", "shadow", "body"]
                },
                modeNode: {
                    id: "mode-node",
                    panel: "mode",
                    type: "button",
                    label: "Node",
                    shortcut: "n",
                    action: _editor.nodeMode
                },
                modeLine: {
                    id: "mode-line",
                    panel: "mode",
                    type: "button",
                    label: "Line",
                    shortcut: "l",
                    action: _editor.lineMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modePolyline: {
                    id: "mode-polyline",
                    panel: "mode",
                    type: "button",
                    label: "Polyline",
                    shortcut: "shift+l",
                    action: _editor.polylineMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modeCircle: {
                    id: "mode-circle",
                    panel: "mode",
                    type: "button",
                    label: "Circle",
                    shortcut: "c",
                    action: _editor.circleMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modePolygon: {
                    id: "mode-polygon",
                    panel: "mode",
                    type: "button",
                    label: "Polygon",
                    shortcut: "shift+p",
                    action: _editor.polygonMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modeQuadraticCurve: {
                    id: "mode-quadratic-curve",
                    panel: "mode",
                    type: "button",
                    label: "Curve",
                    shortcut: "q",
                    action: _editor.quadraticCurveMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modeClosedQuadraticCurve: {
                    id: "mode-closed-quadratic-curve",
                    panel: "mode",
                    type: "button",
                    label: "Loop",
                    shortcut: "shift+q",
                    action: _editor.closedQuadraticCurveMode,
                    tools: ["colour", "fill-rule", "line-style", "line-join", "line-cap", "shadow"]
                },
                modeText: {
                    id: "mode-text",
                    panel: "mode",
                    type: "button",
                    label: "Text",
                    shortcut: "shift+t",
                    action: _editor.textMode,
                    tools: ["text", "colour", "font", "text-align", "text-baseline", "text-direction"]
                },
                modePhysics: {
                    id: "mode-physics",
                    panel: "mode",
                    type: "button",
                    label: "Physics",
                    shortcut: "alt+p",
                    action: _editor.physicsMode,
                    tools: ["physics", "gravity"]
                },
                modeCancel: {
                    id: "mode-cancel",
                    panel: "mode",
                    type: "button",
                    label: "Cancel",
                    icon: "⦰",
                    shortcut: "esc",
                    action: _editor.cancelMode
                },
                transformRotateClockwise: {
                    id: "transform-rotate-clockwise",
                    panel: "transform",
                    type: "button",
                    label: "Rotate Clockwise",
                    icon: "↻",
                    shortcut: "r",
                    action: _editor.rotateClockwise
                },
                transformRotateAnticlockwise: {
                    id: "transform-rotate-anticlockwise",
                    panel: "transform",
                    type: "button",
                    label: "Rotate Anti-Clockwise",
                    icon: "↺",
                    shortcut: "shift+r",
                    action: _editor.rotateAnticlockwise
                },
                transformIncreaseScale: {
                    id: "transform-increase-scale",
                    panel: "transform",
                    type: "button",
                    label: "Increase Scale",
                    icon: "⇱",
                    shortcut: "u",
                    action: _editor.increaseScale
                },
                transformDecreaseScale: {
                    id: "transform-decrease-scale",
                    panel: "transform",
                    type: "button",
                    label: "Decrease Scale",
                    icon: "⇲",
                    shortcut: "shift+u",
                    action: _editor.decreaseScale
                },
                transformIncreaseHorizontalScale: {
                    id: "transform-increase-horizontal-scale",
                    panel: "transform",
                    type: "button",
                    label: "Increase Horizontal Scale",
                    icon: "↦",
                    shortcut: "i",
                    action: _editor.scaleRight
                },
                transformDecreaseHorizontalScale: {
                    id: "transform-decrease-horizontal-scale",
                    panel: "transform",
                    type: "button",
                    label: "Decrease Horizontal Scale",
                    icon: "↤",
                    shortcut: "shift+i",
                    action: _editor.scaleLeft
                },
                transformIncreaseVerticalScale: {
                    id: "transform-increase-vertical-scale",
                    panel: "transform",
                    type: "button",
                    label: "Increase Vertical Scale",
                    icon: "↥",
                    shortcut: "o",
                    action: _editor.scaleUp
                },
                transformDecreaseVerticalScale: {
                    id: "transform-decrease-vertical-scale",
                    panel: "transform",
                    type: "button",
                    label: "Decrease Vertical Scale",
                    icon: "↧",
                    shortcut: "shift+o",
                    action: _editor.scaleDown
                },
                transformSkewLeft: {
                    id: "transform-skew-left",
                    panel: "transform",
                    type: "button",
                    label: "Skew Left",
                    icon: "⇠",
                    shortcut: "j",
                    action: _editor.skewLeft
                },
                transformSkewRight: {
                    id: "transform-skew-right",
                    panel: "transform",
                    type: "button",
                    label: "Skew Right",
                    icon: "⇢",
                    shortcut: "shift+j",
                    action: _editor.skewRight
                },
                transformSkewUp: {
                    id: "transform-skew-up",
                    panel: "transform",
                    type: "button",
                    label: "Skew Up",
                    icon: "⇡",
                    shortcut: "k",
                    action: _editor.skewUp
                },
                transformSkewDown: {
                    id: "transform-skew-down",
                    panel: "transform",
                    type: "button",
                    label: "Skew Down",
                    icon: "⇣",
                    shortcut: "shift+k",
                    action: _editor.skewDown
                },
                colourStroke: {
                    id: "colour-stroke",
                    panel: "colour",
                    type: "text",
                    validator: qd.validator(qd.REGEX.CSS_COLOUR),
                    label: "Stroke",
                    name: "stroke",
                    value: _styles.strokeColour,
                    shortcut: "ctrl+s",
                    action: _editor.stroke,
                    divider: "bar"
                },
                colourFill: {
                    id: "colour-fill",
                    label: "Fill",
                    panel: "colour",
                    type: "text",
                    validator: qd.validator(qd.REGEX.CSS_COLOUR),
                    shortcut: "ctrl+f",
                    name: "fill",
                    value: _styles.fillColour,
                    action: _editor.fill
                },
                colourFillRuleEvenOdd: {
                    id: "colour-fill-rule-evenodd",
                    panel: "fill-rule",
                    type: "radio",
                    label: "Evenodd",
                    shortcut: "ctrl+f+o",
                    name: "fillRule",
                    value: "evenodd",
                    action: _editor.fillRuleEvenOdd
                },
                colourFillRuleNonZero: {
                    id: "colour-fill-rule-non-zero",
                    panel: "fill-rule",
                    type: "radio",
                    label: "Non-Zero",
                    shortcut: "ctrl+f+n",
                    name: "fillRule",
                    value: "nonzero",
                    action: _editor.fillRuleNonZero
                },
                groupSelection: {
                    id: "group-selection",
                    panel: "object",
                    type: "button",
                    label: "Group Selection",
                    icon: "Group",
                    shortcut: "ctrl+g",
                    name: "groupSelection",
                    value: "groupSelection",
                    action: _editor.groupSelection
                },
                ungroupSelection: {
                    id: "ungroup-selection",
                    panel: "object",
                    type: "button",
                    label: "Ungroup Selection",
                    icon: "Ungroup",
                    shortcut: "ctrl+shift+g",
                    name: "ungroupSelection",
                    value: "ungroupSelection",
                    action: _editor.ungroupSelection,
                    divider: "bar"
                },
                raiseSelection: {
                    id: "raise-selection",
                    panel: "object",
                    type: "button",
                    label: "Raise Selection",
                    icon: "Raise",
                    shortcut: "page-up",
                    name: "raiseSelection",
                    value: "raiseSelection",
                    action: _editor.raiseSelection
                },
                raiseSelectionToTop: {
                    id: "raise-selection-to-top",
                    panel: "object",
                    type: "button",
                    label: "Raise to Top",
                    icon: "Top",
                    shortcut: "home",
                    name: "raiseSelectionToTop",
                    value: "raiseSelectionToTop",
                    action: _editor.raiseSelectionToTop
                },
                lowerSelection: {
                    id: "lower-selection",
                    panel: "object",
                    type: "button",
                    label: "Lower Selection",
                    icon: "Lower",
                    shortcut: "page-down",
                    name: "lowerSelection",
                    value: "lowerSelection",
                    action: _editor.lowerSelection
                },
                lowerSelectionToBottom: {
                    id: "lower-selection-to-bottom",
                    panel: "object",
                    type: "button",
                    label: "Lower to Bottom",
                    icon: "Bottom",
                    shortcut: "end",
                    name: "lowerSelectionToBottom",
                    value: "lowerSelectionToBottom",
                    action: _editor.lowerSelectionToBottom
                },
                lineWidth: {
                    id: "line-style-width",
                    panel: "line-style",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Width",
                    shortcut: "ctrl+l",
                    name: "lineWidth",
                    value: _styles.lineWidth,
                    action: _editor.lineWidth,
                    divider: "bar"
                },
                lineDash: {
                    id: "line-dash",
                    panel: "line-style",
                    type: "text",
                    validator: qd.validator(qd.Canvas.STYLES.LINE_DASH.REGEX),
                    label: "Dash",
                    shortcut: "ctrl+d",
                    name: "lineDash",
                    value: _styles.lineDash,
                    action: _editor.lineDash,
                    divider: "bar"
                },
                lineDashOffset: {
                    id: "line-dash-offset",
                    panel: "line-style",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Dash Offset",
                    shortcut: "ctrl+shift+d",
                    name: "lineDashOffset",
                    value: _styles.lineDashOffset,
                    action: _editor.lineDashOffset,
                    divider: "bar"
                },
                lineMiterLimit: {
                    id: "line-miter-limit",
                    panel: "line-style",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Miter Limit",
                    shortcut: "ctrl+m",
                    name: "lineMiterLimit",
                    value: _styles.lineMiterLimit,
                    action: _editor.lineMiterLimit
                },
                lineCapButt: {
                    id: "line-cap-butt",
                    panel: "line-cap",
                    type: "radio",
                    label: "Butt",
                    shortcut: "ctrl+shift+b",
                    name: "lineCap",
                    value: "butt",
                    action: _editor.lineCapButt
                },
                lineCapRound: {
                    id: "line-cap-round",
                    panel: "line-cap",
                    type: "radio",
                    label: "Round",
                    shortcut: "ctrl+shift+r",
                    name: "lineCap",
                    value: "round",
                    action: _editor.lineCapRound
                },
                lineCapSquare: {
                    id: "line-cap-square",
                    panel: "line-cap",
                    type: "radio",
                    label: "Square",
                    shortcut: "ctrl+shift+s",
                    name: "lineCap",
                    value: "square",
                    action: _editor.lineCapSquare
                },
                lineJoinBevel: {
                    id: "line-join-bevel",
                    panel: "line-join",
                    type: "radio",
                    label: "Bevel",
                    shortcut: "ctrl+j+b",
                    name: "lineJoin",
                    value: "bevel",
                    action: _editor.lineJoinBevel
                },
                lineJoinRound: {
                    id: "line-join-round",
                    panel: "line-join",
                    type: "radio",
                    label: "Round",
                    shortcut: "ctrl+j+r",
                    name: "lineJoin",
                    value: "round",
                    action: _editor.lineJoinRound
                },
                lineJoinMiter: {
                    id: "line-join-miter",
                    panel: "line-join",
                    type: "radio",
                    label: "Miter",
                    shortcut: "ctrl+j+m",
                    name: "lineJoin",
                    value: "miter",
                    action: _editor.lineJoinMiter
                },
                textWriter: {
                    id: "text-writer",
                    panel: "text",
                    type: "text",
                    name: "textWriter",
                    value: "",
                    binds: {
                        "keyup": _editor.writeText,
                        "change": _editor.writeText,
                        "focusout": _editor.writeText
                    }
                },
                fontFamily: {
                    id: "font-family",
                    panel: "font",
                    type: "text",
                    validator: qd.validator(qd.REGEX.TEXT),
                    label: "Family",
                    shortcut: "ctrl+t+f",
                    name: "fontFamily",
                    value: _styles.fontFamily,
                    action: _editor.fontFamily,
                    divider: "bar"
                },
                fontSize: {
                    id: "font-size",
                    panel: "font",
                    type: "text",
                    validator: qd.validator(qd.REGEX.CSS_SIZE),
                    label: "Size",
                    shortcut: "ctrl+]/ctrl+[",
                    name: "fontSize",
                    value: _styles.fontSize,
                    action: _editor.fontSize
                },
                textAlignLeft: {
                    id: "text-align-left",
                    panel: "text-align",
                    type: "radio",
                    label: "Left",
                    name: "textAlign",
                    value: "left",
                    shortcut: "ctrl+t+l",
                    action: _editor.textAlignLeft
                },
                textAlignCentre: {
                    id: "text-align-centre",
                    panel: "text-align",
                    type: "radio",
                    label: "Centre",
                    name: "textAlign",
                    value: "centre",
                    shortcut: "ctrl+t+c",
                    action: _editor.textAlignCentre
                },
                textAlignRight: {
                    id: "text-align-right",
                    panel: "text-align",
                    type: "radio",
                    label: "Right",
                    name: "textAlign",
                    value: "right",
                    shortcut: "ctrl+t+r",
                    action: _editor.textAlignRight
                },
                textBaselineTop: {
                    id: "text-baseline-top",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Top",
                    shortcut: "ctrl+t+p",
                    name: "textBaseline",
                    value: "top",
                    action: _editor.textBaselineTop
                },
                textBaselineHanging: {
                    id: "text-baseline-hanging",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Hanging",
                    shortcut: "ctrl+t+h",
                    name: "textBaseline",
                    value: "hanging",
                    action: _editor.textBaselineHanging
                },
                textBaselineMiddle: {
                    id: "text-baseline-middle",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Middle",
                    shortcut: "ctrl+t+m",
                    name: "textBaseline",
                    value: "middle",
                    action: _editor.textBaselineMiddle
                },
                textBaselineAlphabetic: {
                    id: "text-baseline-alphabetic",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Alphabetic",
                    shortcut: "ctrl+t+a",
                    name: "textBaseline",
                    value: "alphabetic",
                    action: _editor.textBaselineAlphabetic
                },
                textBaselineIdeographic: {
                    id: "text-baseline-ideographic",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Ideographic",
                    shortcut: "ctrl+t+i",
                    name: "textBaseline",
                    value: "ideographic",
                    action: _editor.textBaselineIdeographic
                },
                textBaselineBottom: {
                    id: "text-Baseline-bottom",
                    panel: "text-baseline",
                    type: "radio",
                    label: "Bottom",
                    shortcut: "ctrl+t+b",
                    name: "textBaseline",
                    value: "bottom",
                    action: _editor.textBaselineBottom
                },
                textDirectionLTR: {
                    id: "text-direction-ltr",
                    panel: "text-direction",
                    type: "radio",
                    label: "Left-to-Right",
                    shortcut: "ctrl+t+>",
                    name: "textDirection",
                    value: "ltr",
                    action: _editor.textDirectionLTR
                },
                textDirectionRTL: {
                    id: "text-direction-rtl",
                    panel: "text-direction",
                    type: "radio",
                    label: "Right-to-Left",
                    shortcut: "ctrl+t+<",
                    name: "textDirection",
                    value: "rtl",
                    action: _editor.textDirectionRTL
                },
                shadowBlur: {
                    id: "shadow-blur",
                    panel: "shadow",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Blur",
                    shortcut: "ctrl+w+plus/ctrl+w+minus",
                    name: "shadowBlur",
                    value: _styles.shadowBlur,
                    action: _editor.shadowBlur,
                    divider: "bar"
                },
                shadowColour: {
                    id: "shadow-colour",
                    panel: "shadow",
                    type: "text",
                    validator: qd.validator(qd.REGEX.CSS_COLOUR),
                    label: "Colour",
                    shortcut: "ctrl+w+c",
                    name: "shadowColour",
                    value: _styles.shadowColour,
                    action: _editor.shadowColour,
                    divider: "bar"
                },
                shadowOffsetX: {
                    id: "shadow-offset-x",
                    panel: "shadow",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Offset X",
                    shortcut: "ctrl+w+left/ctrl+w+right",
                    name: "shadowOffsetX",
                    value: _styles.shadowOffsetX,
                    action: _editor.shadowOffsetX,
                    divider: "bar"
                },
                shadowOffsetY: {
                    id: "shadow-offset-y",
                    panel: "shadow",
                    type: "text",
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Offset Y",
                    shortcut: "ctrl+w+left/ctrl+w+right",
                    name: "shadowOffsetY",
                    value: _styles.shadowOffsetY,
                    action: _editor.shadowOffsetY
                },
                physicsDampingLinear: {
                    id: "physics-linear-damping",
                    panel: "physics",
                    type: "text",
                    name: "linearDamping",
                    value: _physics._linearDamping,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Linear Damping",
                    action: _editor.linearDamping,
                    divider: "bar"
                },
                physicsDampingRotational: {
                    id: "physics-rotational-damping",
                    panel: "physics",
                    type: "text",
                    name: "rotationalDamping",
                    value: _physics._rotationalDamping,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Rotational Damping",
                    action: _editor.rotationalDamping,
                    divider: "bar"
                },
                physicsGravity: {
                    id: "physics-gravity",
                    panel: "gravity",
                    type: "text",
                    name: "gravity",
                    value: _physics.settings.gravity,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Gravity",
                    action: _editor.gravity,
                    divider: "bar"
                },
                physicsGravitationalConstant: {
                    id: "physics-gravitational-constant",
                    panel: "gravity",
                    type: "text",
                    name: "gravitationalConstant",
                    value: _physics.settings.gravitationalConstant,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Gravitational Constant",
                    action: _editor.gravitationalConstant
                },
                bodyActive: {
                    id: "body-active",
                    panel: "body",
                    type: "text",
                    name: "active",
                    value: true,
                    validator: qd.validator(qd.REGEX.BOOLEAN),
                    label: "Active",
                    action: _editor.bodyActive,
                    divider: "bar"
                },
                bodyDensity: {
                    id: "body-density",
                    panel: "body",
                    type: "text",
                    name: "density",
                    value: 1,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Density",
                    action: _editor.bodyDensity,
                    divider: "bar"
                },
                bodyMoment: {
                    id: "body-moment",
                    panel: "body",
                    type: "text",
                    name: "moment",
                    value: 1,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Moment",
                    action: _editor.bodyMoment,
                    divider: "bar"
                },
                bodyRestitution: {
                    id: "body-restitution",
                    panel: "body",
                    type: "text",
                    name: "restitution",
                    value: 0,
                    validator: qd.validator(qd.REGEX.NUMBER),
                    label: "Restitution",
                    action: _editor.restitution
                },
                pathSmooth: {
                    id: "path-smooth",
                    label: "Smooth Path",
                    shortcut: "ctrl+q+s",
                    action: _editor.smoothPath
                },
                pathunsmooth: {
                    id: "path-unsmooth",
                    label: "Unsmooth Path",
                    shortcut: "ctrl+q+a",
                    action: _editor.unsmoothPath
                },
                toggleToolbar: {
                    id: "toggle-header",
                    label: "Toggle Toolbar",
                    shortcut: "ctrl+h",
                    action: _editor.toggleToolbar
                },
                toggleGrid: {
                    id: "toggle-grid",
                    label: "Toggle Grid",
                    shortcut: "ctrl+alt+g",
                    action: _editor.toggleGrid
                }
            },

            _ContextualToolbar = function (toolbarElement) {
                this._toolbar = toolbarElement;
                this._tools = {};
                this._visible = true;

                this.tool = function (tool) {
                    this._tools[tool.id()] = tool;
                    return this;
                };

                this.visible = function () {
                    return this._visible;
                };

                this.activate = function (toolIds) {
                    qd.forEach(toolIds, function (id) {
                        if (this._tools[id]) {
                            this._tools[id].style("display", "inline");
                        }
                    }, this);

                    return this;
                };

                this.deactivate = function (tools) {
                    qd.forEach(tools, function (id) {
                        if (this._tools[id]) {
                            this._tools[id].style("display", "none");
                        }
                    }, this);

                    return this;
                };

                this.deactivateAll = function () {
                    qd.eachProperty(this._tools, function (id) {
                        this._tools[id].style("display", "none");
                    }, this);

                    return this;
                };

                this.toggle = function () {
                    if (this._visible) {
                        this._toolbar.style("display", "none");
                        this._visible = false;
                    } else if (!this._visible) {
                        this._toolbar.style("display", "block");
                        this._visible = true;
                    }
                };

                this.width = function () {
                    return this._toolbar.width();
                };

                this.height = function () {
                    return this._toolbar.height();
                }
            },

            _contextualToolbar,

            _buildUI = function () {
                var find = qd.Element.find,
                    tag = qd.Element.tag,
                    fldSet = qd.Element.fieldSet,
                    divider = function () {
                        return tag("div").addClass("divider")
                    },
                    makeTitle = function (cmd) {
                        var label = cmd.label || cmd.panel,
                            shortcut = (cmd.shortcut) ? " (" + cmd.shortcut + ")" : "";

                        return qd.capitalise(label.concat(shortcut));
                    },
                    fld = function (cmd) {
                        var id = cmd.id,
                            type = cmd.type,
                            label = (cmd.icon) ? cmd.icon : cmd.label,
                            title = makeTitle(cmd),
                            name = (cmd.name) ? cmd.name : qd.shrink(label.toLowerCase()),
                            value = (qd.isDefinedAndNotNull(cmd.value)) ? cmd.value : name,
                            action = cmd.action,
                            binds = cmd.binds,
                            field;
                        
                        switch(type) {
                            case "button":
                                field = qd.Element.button(id, label, title, name, value);

                                if (action) {
                                    field.bind("click", action);
                                }
                                
                                break;
                            case "text":
                                var textLabel = qd.Element.tag("span").text(label),
                                    input = qd.Element.tag("input", {
                                        id: id,
                                        attrs: {
                                            type: "text",
                                            name: name,
                                            value: ((qd.isDefinedAndNotNull(value)) ? value : name),
                                            title: title
                                        }
                                    });

                                field = qd.Element.tag("span").append(textLabel).append(input);

                                input.bind("focus", function () {
                                    _keyboardCtx.disable();
                                }).bind("blur", function () {
                                    _keyboardCtx.enable();
                                });

                                if (action) {
                                    input.bind("keyup", function () {
                                        action();
                                    });
                                }

                                break;
                            case "radio":
                                field = qd.Element.radio(id, label, title, (value === _styles[name]), name, value);

                                if (action) {
                                    field.bind("click", action);
                                }

                                break;
                            case "file":
                                field = qd.Element.file(id, label, title, name, value);
                                if (action) {
                                    field.bind("change", action);
                                }

                                break;
                            default:
                                field = qd.Element.input(id, type, title, name, value);
                            
                        }

                        if (qd.isDefinedAndNotNull(binds)) {
                            qd.eachProperty(binds, function (event, callback) {
                                field.bind(event, callback);
                            });
                        }

                        return field;
                    },
                    bar = tag("div").id("bar").append(
                        tag("header").id("logo").attr("title", "Quick & Dirty").append(
                            tag("img").attr("src", "images/qd.svg"))
                    ),
                    tools = tag("div").id("tools"),
                    panels = {};

                bar.append(tools);

                qd.eachProperty(_commands, function (cmdId, cmd) {
                    var panelId = cmd.panel,
                        panel;

                    if (qd.isDefinedAndNotNull(panelId)) {
                        panel = panels[panelId];

                        if (qd.isUndefinedOrNull(panel)) {
                            // Create panel first time
                            panel = fldSet(panelId, qd.capitalise(panelId));
                            tools.append(panel);
                            panels[panelId] = panel;
                        }

                        panel.append(fld(cmd));

                        if (qd.isDefinedAndNotNull(cmd.divider)) {
                            panel.append(divider());
                        }

                        panel.draggable();
                    }
                });

                find("body").prepend(bar);

                _contextualToolbar = new _ContextualToolbar(bar)
                    .tool(panels["edit"])
                    .tool(panels["transform"])
                    .tool(panels["colour"])
                    .tool(panels["fill-rule"])
                    .tool(panels["object"])
                    .tool(panels["line-style"])
                    .tool(panels["line-cap"])
                    .tool(panels["line-join"])
                    .tool(panels["text"])
                    .tool(panels["font"])
                    .tool(panels["text-align"])
                    .tool(panels["text-baseline"])
                    .tool(panels["text-direction"])
                    .tool(panels["shadow"])
                    .tool(panels["physics"])
                    .tool(panels["gravity"])
                    .tool(panels["body"])
                    .deactivateAll();

                return this;
            },

            _Mode = function (command) {

                /* Private attributes */

                var _this = this,
                    _namespace = "qd.Mode.".concat(qd.shrink(command.label)),
                    _modeMouseCtx = new qd.MouseContext(_mouse, _namespace, _this),
                    _modeKeyboardCtx = new qd.KeyboardContext(_keyboard, _namespace, _this),

                /* Private functions */

                    _execute = function (fnName) {
                        var fn = _this.fn[fnName];

                        if (fn) {
                            fn.call(_this);
                        }
                    },

                    _init = function () {
                        _execute("init");
                        _start();
                    },

                    _start = function () {
                        _execute("start");
                    },

                    _finish = function () {
                        _execute("finish");
                    };

                /* Public attributes */

                this.label = command.label;
                this.d = {};    // data
                this.fn = {};   // functions
                this.activated = false;
                this.tools = command.tools || [];

                /* Public fn */

                this.init = function (fn) {
                    this.fn["init"] = fn;
                    return this;
                };

                this.start = function (fn) {
                    this.fn["start"] = fn;
                    return this;
                };

                this.finish = function (fn) {
                    this.fn["finish"] = fn;
                    return this;
                };

                this.bind = function () {
                    var args = new qd.Args(arguments),
                        event,
                        jwertyCode,
                        callback;

                    if (args.matches(String, Function)) {
                        event = args.get(0);
                        callback = args.get(1);

                        _modeMouseCtx.bind(event, callback);
                    } else if (args.matches(String, String, Function)) {
                        event = args.get(0);
                        jwertyCode = args.get(1);
                        callback = args.get(2);

                        _modeKeyboardCtx.bind(event, jwertyCode, callback);
                    }

                    return this;
                };

                this.activate = function () {
                    _init();
                    _modeMouseCtx.enable();
                    _modeKeyboardCtx.enable();
                    _contextualToolbar.activate(this.tools);
                    if (_contextualToolbar.visible()) {
                        _editor.resizeToClientWindow();
                    }
                    this.activated = true;
                    return this;
                };

                this.deactivate = function () {
                    _finish();
                    _modeMouseCtx.disable();
                    _modeKeyboardCtx.disable();
                    _contextualToolbar.deactivateAll();
                    this.activated = false;
                    return this;
                };

                this.restart = function () {
                    _start();
                };

                this.cancel = function () {
                    _finish();
                    _init();
                    return this;
                };

                this.data = function (d) {
                    if (d) {
                        this.d = qd.mergeProperties(this.d, d);
                        return this;
                    }

                    else return d;
                };

                this.function = function (name, fn) {
                    if (name) {
                        if (fn) {
                            this.fn[name] = function () {
                                return fn.apply(_this, arguments);
                            };

                            return this;
                        } else {
                            return this.fn[name];
                        }
                    }

                    return this;
                };

                this.functions = function (fns) {
                    if (fns) {
                        qd.eachProperty(fns, function (name, fn) {
                            this.function(name, fn);
                        }, this);

                        return this;
                    }

                    return this.fn;
                };

                this.drawer = function (fn) {
                    this.fn["draw"] = fn;
                    return this;
                };

                this.draw = function (canvas) {
                    var fn = this.fn["draw"];

                    if (fn && this.activated) {
                        fn.call(_this, canvas);
                    }

                    return this;
                };

                this.mouse = function () {
                    return _modeMouseCtx.mouse();
                };

                this.keyboard = function () {
                    return _modeKeyboardCtx.keyboard();
                };
            },

        /* Draw Line Mode */
            _lineMode = new _Mode(_commands.modeLine)
                .data({
                    timeStep: 0,
                    pointA: qd.Point2D.create(0, 0),
                    pointB: qd.Point2D.create(0, 0)
                })
                .start(function () {
                    this.d.click = 0;
                })
                .bind("click", function (mouse) {
                    var d = this.d;

                    d.click += 1;

                    if (d.click === 1) {
                        d.pointA = mouse.worldPoint();
                    } else if (d.click === 2) {
                        d.pointB = mouse.worldPoint();
                        _addNewEntity(_view.line(d.pointA, d.pointB));
                        this.restart();
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        pointA,
                        pointB;

                    if (d.click === 1) {
                        pointA = d.pointA;
                        pointB = _view.mouseWorldPoint(_mouse);
                        _view.beginPath()
                            .traceLine(pointA[0], pointA[1], pointB[0], pointB[1])
                            .drawPath(_DASHED);
                    }
                }),

        /* Draw Circle Mode */
            _circleMode = new _Mode(_commands.modeCircle)
                .start(function () {
                    this.d.click = 0;
                    this.d.centre = null;
                })
                .bind("click", function (mouse) {
                    var d = this.d,
                        point = mouse.worldPoint();

                    d.click += 1;

                    if (d.click === 1) {
                        d.centre = point;
                    } else if (d.click === 2) {
                        var centre = d.centre,
                            radius = qd.Point2D.distance(centre, point),
                            ellipse = _view.circle(centre, radius, radius)
                                .styles(_styles);

                        _addNewEntity(ellipse);
                        this.restart();
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        centre,
                        mousePoint,
                        radius;

                    if (d.click === 1) {
                        centre = d.centre;
                        mousePoint = _mouse.worldPoint();
                        radius = qd.Point2D.distance(centre, mousePoint);

                        canvas.beginPath();
                        _view.traceCircle(centre[0], centre[1], radius);
                        _view.traceLine(centre[0], centre[1], mousePoint[0], mousePoint[1]);
                        canvas.drawPath(_DASHED);
                    }
                }),

        /* Draw Polygon Mode */
            _polygonMode = new _Mode(_commands.modePolygon)
                .start(function () {
                    this.d.click = 0;
                    this.d.points = [];
                })
                .bind("click", function (mouse) {
                    this.d.click += 1;
                    var point = _view.mouseWorldPoint(mouse);
                    this.d.points.push(point);
                })
                .bind("keydown", "enter", function () {
                    if (this.d.points.length > 1) {
                        _addNewEntity(_view.polygon(this.d.points));
                        this.restart();
                    }
                })
                .bind("dblclick", function () {
                    if (this.d.points.length > 1) {
                        _addNewEntity(_view.polygon(this.d.points));
                        this.restart();
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        lastPoint,
                        mousePoint;

                    if (d.click > 0) {
                        _view.beginPath();

                        if (d.points.length > 1) {
                            _view.tracePolyline(d.points);
                        }

                        lastPoint = d.points[d.points.length - 1];
                        mousePoint = _view.mouseWorldPoint(_mouse);
                        _view.traceLine(lastPoint[0], lastPoint[1], mousePoint[0], mousePoint[1]);
                        _view.drawPath(_DASHED);
                    }
                }),

        /* Draw Polyline Mode */
            _polylineMode = new _Mode(_commands.modePolyline)
                .start(function () {
                    this.d.click = 0;
                    this.d.points = [];
                })
                .function("lastPoint", function (closed) {
                    if (closed) {
                        _addNewEntity(_view.polygon(this.d.points));
                    } else {
                        _addNewEntity(_view.polyline(this.d.points));
                    }

                    this.restart();
                })
                .bind("click", function (mouse) {
                    this.d.click += 1;
                    var point = _view.mouseWorldPoint(mouse);
                    this.d.points.push(point);
                })
                .bind("keydown", "enter", function (event) {
                    if (this.d.points.length > 1) {
                        this.fn["lastPoint"](_keyboard.is("shift"));
                    }
                })
                .bind("dblclick", function (event) {
                    if (this.d.points.length > 1) {
                        this.fn["lastPoint"](_keyboard.is("shift"));
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        lastPoint,
                        mousePoint;

                    if (d.click > 0) {
                        canvas.beginPath();

                        if (d.points.length > 1) {
                            _view.tracePolyline(d.points);
                        }

                        lastPoint = d.points[d.points.length - 1];
                        mousePoint = _view.mouseWorldPoint(_mouse);
                        _view.traceLine(lastPoint[0], lastPoint[1], mousePoint[0], mousePoint[1]);
                        canvas.drawPath(_DASHED);
                    }
                }),

        /* Pen Mode */
            _penMode = new _Mode(_commands.modePen)
                .init(function () {
                    this.mouse().cursor("pointer");
                })
                .start(function () {
                    this.d.click = 0;
                    this.d.points = [];
                    this.d.prevPoint = null;
                })
                .bind("mousedown", function (mouse) {
                    var point;

                    if (this.d.click == 1) {
                        point = _view.mouseWorldPoint(mouse);
                        this.d.points.push(point);
                        this.d.prevPoint = point;
                    }

                    this.d.click += 1;
                })
                .bind("move", function (mouse) {
                    var point;

                    if (this.d.click > 0 ) {
                        point = _view.mouseWorldPoint(mouse);

                        if (!qd.Point2D.equals(this.d.prevPoint, point)) {
                            this.d.points.push(_view.mouseWorldPoint(mouse));
                            this.d.prevPoint = point;
                        }
                    }
                })
                .bind("mouseup", function () {
                    var curve;
                    if (this.d.points.length > 2) {
                        if (_keyboard.is("shift")) {
                            curve = _view.closedQuadraticCurve(this.d.points);
                        } else {
                            curve = _view.quadraticCurve(this.d.points);
                        }

                        _addNewEntity(curve);
                    }

                    this.restart();
                })
                .drawer(function (canvas) {
                    if (this.d.click > 0) {

                        if (this.d.points.length > 2) {
                            _view.beginPath()
                                .traceQuadraticCurve(this.d.points)
                                .drawPath(_DASHED);
                        }
                    }
                })
                .finish(function () {
                    this.mouse().cursor("default");
                }),

        /* Quadratic Curve Mode */
            _quadraticCurveMode = new _Mode(_commands.modeQuadraticCurve)
                .start(function () {
                    this.d.click = 0;
                    this.d.points = [];
                })
                .function("addLastPoint", function (closed) {
                    var curve;

                    if (closed) {
                        curve = _view.closedQuadraticCurve(this.d.points);
                    } else {
                        curve = _view.quadraticCurve(this.d.points);
                    }

                    _addNewEntity(curve);
                    this.restart();
                })
                .bind("click", function (mouse) {
                    this.d.click += 1;
                    this.d.points.push(_view.mouseWorldPoint(mouse));
                })
                .bind("keydown", "enter", function () {
                    if (this.d.points.length > 1) {
                        this.fn["addLastPoint"](false);
                    }
                })
                .bind("keydown", "shift+enter", function () {
                    if (this.d.points.length > 1) {
                        this.fn["addLastPoint"](true);
                    }
                })
                .bind("dblclick", function () {
                    var closed = false;

                    if (this.d.points.length > 1) {
                        if (_keyboard.is("shift")) {
                            closed = true;
                        }

                        this.fn["addLastPoint"](closed);
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        points = d.points,
                        pointCount = points.length,
                        lastPoint,
                        mousePoint;

                    if (d.click > 0) {

                        _view.beginPath();

                        if (pointCount === 2) {
                            _view.tracePolyline(points)
                        } else if (pointCount > 2) {
                            _view.traceQuadraticCurve(points);
                        }

                        lastPoint = points[pointCount - 1];
                        mousePoint = _view.mouseWorldPoint(this.mouse());

                        _view.traceLine(lastPoint[0], lastPoint[1], mousePoint[0], mousePoint[1]);
                        _view.drawPath(_DASHED);
                    }
                }),

        /* Closed Quadratic Curve Mode */
            _closedQuadraticCurveMode = new _Mode(_commands.modeClosedQuadraticCurve)
                .start(function () {
                    this.d.click = 0;
                    this.d.points = [];
                })
                .bind("click", function (mouse) {
                    this.d.click += 1;
                    this.d.points.push(_view.mouseWorldPoint(mouse));
                })
                .bind("keydown", "enter", function () {
                    if (this.d.points.length > 1) {
                        _addNewEntity(_view.closedQuadraticCurve(this.d.points));
                        this.restart();
                    }
                })
                .bind("dblclick", function () {
                    if (this.d.points.length > 1) {
                        _addNewEntity(_view.closedQuadraticCurve(this.d.points));
                        this.restart();
                    }
                })
                .drawer(function (canvas) {
                    var d = this.d,
                        points = d.points,
                        pointCount = points.length,
                        lastPoint,
                        mousePoint;

                    if (d.click > 0) {

                        _view.beginPath();

                        if (pointCount === 2) {
                            _view.tracePolyline(points)
                        } else if (pointCount > 2) {
                            _view.traceQuadraticCurve(points);
                        }

                        lastPoint = this.d.points[this.d.points.length - 1];
                        mousePoint = _view.mouseWorldPoint(this.mouse());
                        _view.traceLine(lastPoint[0], lastPoint[1], mousePoint[0], mousePoint[1]);
                        _view.drawPath(_DASHED);
                    }
                }),

            _textMode = new _Mode(_commands.modeText)
                .bind("init", function () {

                })
                .bind("finish", function () {

                }),

        /* Select Mode */
            _selectMode = new _Mode(_commands.modeSelect)
                .data({
                    dragStart: null
                })
                .bind("keydown", "ctrl+a", function () {
                    _selected.addAll(_world.selectable());
                })
                .bind("dblclick", function (mouse) {
                    var selectable = _world.selectable(),
                        entity = selectable.clickTest(mouse),
                        graphic = entity.graphic(0);

                    if (graphic instanceof qd.Text) {
                        _switchMode(_textMode);

                        var textInput = qd.Element.getById(_commands.textWriter.id);
                        textInput.get().focus();
                        textInput.value(graphic.text());
                        _writingPad.writer(graphic, function (text) {
                            this.text(_canvas, text);
                        });
                    }
                })
                .bind("mousedown", function (mouse) {
                    var selectable = _world.selectable(),
                        entity = selectable.clickTest(mouse);

                    if (qd.isDefinedAndNotNull(entity)) {
                        if (_keyboard.is("shift")) {
                            _selected.add(entity);
                            this.d.dragStart = _view.mouseWorldPoint(mouse);
                        } else if (this.d.dragStart === null) {
                            _selected.clear();
                            _selected.put(entity);
                        }
                    } else {
                        this.d.dragStart = _view.mouseWorldPoint(mouse);
                        _selected.clear();
                    }
                })
                .bind("mouseup", function (mouse) {
                    var dragStart = this.d.dragStart,
                        dragEnd,
                        dragRect;

                    if (dragStart) {
                        dragEnd = _view.mouseWorldPoint(_mouse);
                        dragRect = new qd.math.Rectangle(dragStart, dragEnd);

                        _world.selectable().each(function (entity) {
                            if (entity.bounds().boxCollisionTest(dragRect.min, dragRect.max)) {
                                _selected.add(entity);
                            }
                        });
                    }

                    this.d.dragStart = null;
                })
                .drawer(function (canvas) {
                    var dragStart,
                        dragEnd,
                        dragRect;

                    _view.beginPath();

                    // Draw Selection Box
                    dragStart = this.d.dragStart

                    if (dragStart) {
                        dragEnd = _view.mouseWorldPoint(_mouse);
                        dragRect = new qd.math.Rectangle(dragStart, dragEnd);

                        _view.traceRectangle(dragRect.min[0], dragRect.min[1], dragRect.width, dragRect.height);

                        // Draw highlighted entities inside selection box
                        _world.selectable().each(function (entity) {
                            var bs;

                            if (entity.bounds().boxCollisionTest(dragRect.min, dragRect.max)) {
                                bs = entity.bounds();
                                _view.traceRectangle(bs.left(), bs.top(), bs.width(), bs.height());
                            }
                        });
                    }

                    // Draw previously selected entities
                    _selected.each(function (entity) {
                        var bs = entity.bounds();
                        _view.traceRectangle(bs.left(), bs.top(), bs.width(), bs.height());
                    });

                    _view.drawPath(_DASHED)
                })
                .finish(function () {
                    _selected.clear();
                    this.d.dragStart = null;
                }),

        /* Edit Path Mode */
            _nodeMode = new _Mode(_commands.modeNode)
                .init(function () {

                })
                .bind("click", function (mouse) {
                    if (_keyboard.is("shift")) {
                        _handleSet.selectAdd(mouse);
                    } else {
                        _handleSet.select(mouse);
                    }
                })
                .bind("keydown", "ctrl+a", function () {
                    _handleSet.selectAll(true);
                })
                .bind("keydown", "w/up", function () {
                    _handleSet.translate(0, -_moveStep());
                })
                .bind("keydown", "s/down", function () {
                    _handleSet.translate(0, _moveStep());
                })
                .bind("keydown", "a/left", function () {
                    _handleSet.translate(-_moveStep(), 0);
                })
                .bind("keydown", "d/right", function () {
                    _handleSet.translate(_moveStep(), 0);
                })
                .drawer(function (canvas) {
                    _handleSet.draw(canvas);
                })
                .finish(function () {
                    _handleSet.selectAll(false);
                }),

        /* Physics Mode */
            _physicsMode = new _Mode(_commands.modePhysics),

            _moveStep = function () {
                return (_MOVE_STEP * _camera.zoomLevel());
            },

            _switchMode = function (mode) {
                _mode.deactivate();
                _mode = mode;
                _mode.activate();
                return this;
            },

            _mode = _lineMode,

            _addNewEntity = function (graphic) {
                graphic.shape().handles(_handleSet);
                graphic.styles(_styles);
                return _world.createEntity(graphic).selectable().draggable();
            },

            _set = function (command, setter) {
                var element = qd.Element.getById(command.id),
                    name = command.name,
                    value = element.value(),
                    validator = command.validator;

                if (validator) {
                    if (validator.validate(value)) {
                        setter(name, value);
                        element.style("backgroundColor", qd.Colour.WHITE)
                    } else {
                        element.style("backgroundColor", qd.Colour.MISTY_ROSE)
                    }
                } else {
                    setter(name, value);
                }

                return _editor;
            },

            _applyStyle = function (command) {
                return _set(command, function (style, value) {
                    _styles[style] = value;
                    _selected.applyStyle(style, value);
                });
            },

            _applyPhysicsConstant = function (command) {
                return _set(command, function (constant, value) {
                    _physics.setting(constant, value);
                });
            },

            _applyBodyProperty = function (command) {
                return _set(command, function (property, value) {
                    _selected.applyBodyProperty(property, value);
                });
            },

            _mouseCtx = new qd.MouseContext(_mouse, "qd.Editor", this),

            _bindMouseCommands = function (mouseCtx) {
                mouseCtx.bind("wheel", function (mouse, event) {
                    if (mouse.wheel(event) > 0) {
                        _editor.zoomIn();
                    } else if (mouse.wheel(event) < 0) {
                        _editor.zoomOut();
                    }

                    event.preventDefault();
                })
                .enable();
            },

            _keyboardCtx = new qd.KeyboardContext(_keyboard, "qd.Editor", this),

            _bindKeyboardCommands = function (keyboardCtx, commands) {
                qd.eachProperty(commands, function (name, cmd) {
                    keyboardCtx.bind("keydown", cmd.shortcut, (cmd.shortcutHandler) ? cmd.shortcutHandler : cmd.action);
                });

                keyboardCtx.preventDefault("keydown", "down-arrow")
                    .preventDefault("keydown", "up-arrow")
                    .preventDefault("keydown", "left-arrow")
                    .preventDefault("keydown", "right-arrow")
                    .preventDefault("keydown", "delete");

                keyboardCtx.enable();
            },

            _bindWindowEvents = function () {
                qd.Element.getById("canvas").droppable().bind("drop", function (event) {
                    _editor.dropImage(event);
                });

                window.addEventListener("resize", _editor.resizeToClientWindow);

                window.addEventListener("blur", function () {
                    _engine.pause();
                });

                window.addEventListener("focus", function () {
                    _engine.run();
                });
            },

            /** Add temporary testing code here */
            _playground = function () {

            },

            _init = function () {
                _buildUI();
                _bindMouseCommands(_mouseCtx);
                _bindKeyboardCommands(_keyboardCtx, _commands);
                _bindWindowEvents();
                _mode.activate();
                _grid.draw(_camera);
                _playground();
                _engine.run(_editor.draw, _editor.step);
            };

        _init();
    };

    return qd;

}(qd));
