(function (qd) {

    /**
     * Canvas is a wrapper for HTMLCanvasElement and its CanvasRenderingContext2D.
     *
     * It provides proxy functions for most rendering operations.
     *
     * @param {Object} settings
     */
    qd.Canvas = function (settings) {
        this.init(settings);
    };

    /** Static */

    /**
     * Complete list of canvas styles.
     *
     * @type {Object}
     */
    qd.Canvas.STYLES = {

        /* CanvasRenderingContext2D styles */

        STROKE_COLOUR: {
            NAME: "strokeColour",
            TYPE: String,
            DEFAULT: "black"
        },
        STROKE_GRADIENT: {
            NAME: "strokeGradient",
            TYPE: qd.Gradient,
            DEFAULT: "flat"
        },
        STROKE_PATTERN: {
            NAME: "strokePattern",
            TYPE: qd.Pattern,
            DEFAULT: "flat"
        },
        FILL_COLOUR: {
            NAME: "fillColour",
            TYPE: String,
            DEFAULT: "transparent"
        },
        FILL_GRADIENT: {
            NAME: "fillGradient",
            TYPE: qd.Gradient,
            DEFAULT: "flat"
        },
        FILL_PATTERN: {
            NAME: "fillPattern",
            TYPE: qd.Pattern,
            DEFAULT: "flat"
        },
        FILL_RULE: {
            NAME: "fillRule",
            TYPE: String,
            VALUES: ["nonzero", "evenodd"],
            DEFAULT: "nonzero"
        },
        LINE_WIDTH: {
            NAME: "lineWidth",
            TYPE: Number,
            DEFAULT: 1.0
        },
        LINE_CAP: {
            NAME: "lineCap",
            TYPE: String,
            VALUES: ["butt", "round", "square"],
            DEFAULT: "butt"
        },
        LINE_JOIN: {
            NAME: "lineJoin",
            TYPE: String,
            VALUES: ["round", "bevel", "miter"],
            DEFAULT: "miter"
        },
        LINE_MITER_LIMIT: {
            NAME: "lineMiterLimit",
            TYPE: Number,
            DEFAULT: 10
        },
        LINE_DASH: {
            NAME: "lineDash",
            TYPE: Array,
            DEFAULT: [],
            REGEX: /^(\d)+(, *(\d)+)*$|^(solid)$/
        },
        LINE_DASH_OFFSET: {
            NAME: "lineDashOffset",
            TYPE: Number,
            DEFAULT: 0
        },
        FONT_SIZE: {
            NAME: "fontSize",
            TYPE: String,
            DEFAULT: "10px"
        },
        FONT_FAMILY: {
            NAME: "font",
            TYPE: String,
            DEFAULT: "sans-serif"
        },
        TEXT_ALIGN: {
            NAME: "textAlign",
            TYPE: String,
            VALUES: ["left", "right", "center"],
            DEFAULT: "left"
        },
        TEXT_BASELINE: {
            NAME: "textBaseline",
            TYPE: String,
            VALUES: ["top", "hanging", "middle", "alphabetic", "bottom"],
            DEFAULT: "alphabetic"
        },
        TEXT_DIRECTION: {
            NAME: "textDirection",
            TYPE: String,
            VALUES: ["ltr", "rtl"],
            DEFAULT: "ltr"
        },
        SHADOW_BLUR: {
            NAME: "shadowBlur",
            TYPE: Number,
            DEFAULT: 0
        },
        SHADOW_COLOUR: {
            NAME: "shadowColour",
            TYPE: String,
            DEFAULT: "black"
        },
        SHADOW_OFFSETX: {
            NAME: "shadowOffsetX",
            TYPE: Number,
            DEFAULT: 0
        },
        SHADOW_OFFSETY: {
            NAME: "shadowOffsetY",
            TYPE: Number,
            DEFAULT: 0
        },

        /* qd.Canvas styles */
        ARROW_WIDTH: {
            NAME: "arrowTipWidth",
            TYPE: Number,
            DEFAULT: 9
        },

        ARROW_TIP: {
            NAME: "arrowTip",
            TYPE: String,
            VALUES: ["none", "open", "closed", "invertedOpen", "invertedClosed"],
            DEFAULT: "open"
        },

        ARROW_BASE: {
            NAME: "arrowBase",
            TYPE: String,
            VALUES: ["none", "open", "closed", "invertedOpen", "invertedClosed"],
            DEFAULT: ["none"]
        }
    };

    /**
     * Create a property map of all the default styles.
     *
     * @return {Object} property map of all the default styles
     * @constructor
     */
    qd.Canvas.CREATE_DEFAULT_STYLES = function () {
        return qd.Canvas.SET_TO_DEFAULT_STYLES({});
    };

    /**
     * Set all the styles to their defaults.
     *
     * @param {Object} styles
     * @return {Object} styles
     * @constructor
     */
    qd.Canvas.SET_TO_DEFAULT_STYLES = function (styles) {
        qd.eachProperty(qd.Canvas.STYLES, function (id, style) {
            styles[style.NAME] = style.DEFAULT;
        });

        return styles;
    };

    /** Private Methods */

    qd.Canvas.prototype._getCanvas = function (canvas) {
        var canvasElem = null;

        if (typeof canvas === "string") {
            canvasElem = qd.getElementById(canvas);
        } else if (canvas instanceof HTMLCanvasElement) {
            canvasElem = canvas;
        } else {
            throw new Error("Failed to create the qd.Canvas because cannot find the HTMLCanvasElement.")
        }

        return canvasElem;
    };

    qd.Canvas.prototype._getContext2D = function (canvasElem) {
        var ctx = canvasElem.getContext("2d");

        if (qd.isUndefinedOrNull(ctx)) {
            throw new Error("2D Canvas not supported.")
        }

        return ctx;
    };

    /** Public Methods */

    /**
     * Default initialisation function.
     *
     * @param {Object} settings
     */
    qd.Canvas.prototype.init = function (settings) {
        this._settings = qd.mergeProperties({
            canvas: "canvas"
        }, settings || {});

        this._canvas = this._getCanvas(this._settings.canvas);

        this._ctx = this._getContext2D(this._canvas);

        this._styles = new qd.Styles(qd.Canvas.CREATE_DEFAULT_STYLES());

        this._view = new qd.View(this._canvas.width, this._canvas.height);
    };

    qd.Canvas.prototype.view = function () {
        this._view.canvas(this);
        return this._view;
    };

    /**
     * Get the CanvasRenderingContext2D.
     *
     * @return {CanvasRenderingContext2D} the CanvasRenderingContext2D
     */
    qd.Canvas.prototype.context = function () {
        return this._ctx;
    };

    /**
     * Clear all the graphics from the canvas area.
     *
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.clear = function () {
        this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
        return this;
    };

    /**
     * Clear graphics in a rectangle.
     *
     * @param {Number} x
     * @param {Number} y
     * @param {Number} width
     * @param {Number} height
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.clearRectangle = function (x, y, width, height) {
        this._ctx.clearRect(x, y, width, height);
        return this;
    };

    /**
     * Get the x position of canvas area's centre point.
     *
     * @return {Number} centre x within the canvas width
     */
    qd.Canvas.prototype.centreX = function () {
        return Math.floor(this._canvas.width / 2);
    };

    /**
     * Get the y position of canvas area's centre point.
     *
     * @return {Number} centre y within the canvas height
     */
    qd.Canvas.prototype.centreY = function () {
        return Math.floor(this._canvas.height / 2);
    };

    /**
     * Get the canvas area's centre point.
     *
     * @return {qd.Point2D}
     */
    qd.Canvas.prototype.centre = function () {
        return qd.Point2D.create(this.centreX(), this.centreY());
    };

    /**
     * Get the x position of a random point in the canvas area.
     *
     * @return {Number} random x within the canvas width
     */
    qd.Canvas.prototype.randomX = function () {
        return Math.floor(Math.random() * this._canvas.width);
    };

    /**
     * Get the y position of a random point in the canvas area.
     *
     * @return {Number} random y within the canvas height
     */
    qd.Canvas.prototype.randomY = function () {
        return Math.floor(Math.random() * this._canvas.height);
    };

    /**
     * Get a random point in the canvas.
     *
     * @return {qd.Point2D} random point within the canvas
     */
    qd.Canvas.prototype.randomPoint = function () {
        return qd.Point2D.create(this.randomX(), this.randomY());
    };

    /**
     * Get the canvas width.
     *
     * @return {Number}
     */
    qd.Canvas.prototype.width = function () {
        return this._canvas.width;
    };

    /**
     * Get the canvas height.
     *
     * @return {Number}
     */
    qd.Canvas.prototype.height = function () {
        return this._canvas.height;
    };

    /**
     * Begin tracing paths to the canvas.
     *
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.beginPath = function () {
        this._ctx.beginPath();
        return this;
    };

    /**
     * Start tracing a path at the {@code point}.
     *
     * @param {qd.Point2D} point
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.moveTo = function (point) {
        this._ctx.moveTo(point[0], point[1]);
        return this;
    };

    /**
     * Trace a line path to {@code point}.
     *
     * @param {qd.Point2D} point
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.lineTo = function (point) {
        this._ctx.lineTo(point[0], point[1]);
        return this;
    };

    /**
     * Trace a line path from coordinates {@code (x0, y0)} to {@code (x1, y1)}.
     *
     * @param {Number} x0
     * @param {Number} y0
     * @param {Number} x1
     * @param {Number} y1
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceLine = function (x0, y0, x1, y1) {
        var ctx = this._ctx;

        ctx.moveTo(x0, y0);
        ctx.lineTo(x1, y1);
        return this;
    };

    /**
     * Trace a rectangle path.
     *
     * @param {Number} x
     * @param {Number} y
     * @param {Number} width
     * @param {Number} height
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceRectangle = function (x, y, width, height ) {
        var ctx = this._ctx;

        ctx.rect(x, y, width, height);
        return this;
    };

    /**
     * Trace a circular arc path.
     *
     * @param {Number} x
     * @param {Number} y
     * @param {Number} radius
     * @param {Number} startAngle
     * @param {Number} endAngle
     * @param {Boolean} anticlockwise
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceCircularArc = function (x, y, radius, startAngle, endAngle, anticlockwise) {
        this._ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise);
        return this;
    };

    /**
     * Draw a circle positioned at (x, y) with the specified radius.
     *
     * @param {Number} x
     * @param {Number} y
     * @param {Number} radius
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceCircle = function (x, y, radius) {
        this._ctx.arc(x, y, radius, 0, qd.math.TAU, true);
        return this;
    };

    /**
     * Trace an ellipse.
     *
     * @param {Number} x
     * @param {Number} y
     * @param {Number} radiusX
     * @param {Number} radiusY
     * @param {Number} rotation
     * @param {Number} startAngle
     * @param {Number} endAngle
     * @param {Boolean} anticlockwise
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceEllipse = function (x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise) {
        this._ctx.ellipse(x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise);
        return this;
    };

    /**
     * Trace an ellipse path using four sets of bezier curves, one for each quadrant of the unit circle.
     *
     * {@code points} must contain exactly 13 points:
     *   - The first point is the starting point
     *   - Four sets of three points, each of which describe a bezier curve
     *
     *  Each set of three points contain:
     *    - Two control points
     *    - A terminal point
     *
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceEllipseAsBezierCurves = function (points) {
        // Ellipse code taken from: http://stackoverflow.com/questions/2172798/how-to-draw-an-oval-in-html5-canvas
        var ctx = this._ctx,
            p0,

            q1cp1,
            q1cp2,
            q1p,

            q2cp1,
            q2cp2,
            q2p,

            q3cp1,
            q3cp2,
            q3p,

            q4cp1,
            q4cp2,
            q4p;

        if (points.length !== 13) {
            return this;
        }

        p0 = points[0];

        q1cp1 = points[1];
        q1cp2 = points[2];
        q1p = points[3];

        q2cp1 = points[4];
        q2cp2 = points[5];
        q2p = points[6];

        q3cp1 = points[7];
        q3cp2 = points[8];
        q3p = points[9];

        q4cp1 = points[10];
        q4cp2 = points[11];
        q4p = points[12];

        ctx.moveTo(p0[0], p0[1]);
        ctx.bezierCurveTo(q1cp1[0], q1cp1[1], q1cp2[0], q1cp2[1], q1p[0], q1p[1]);
        ctx.bezierCurveTo(q2cp1[0], q2cp1[1], q2cp2[0], q2cp2[1], q2p[0], q2p[1]);
        ctx.bezierCurveTo(q3cp1[0], q3cp1[1], q3cp2[0], q3cp2[1], q3p[0], q3p[1]);
        ctx.bezierCurveTo(q4cp1[0], q4cp1[1], q4cp2[0], q4cp2[1], q4p[0], q4p[1]);

        return this;
    };

    /**
     * Trace a polyline.
     *
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.tracePolyline = function (points) {
        var ctx = this._ctx,
            i,
            point = points[0];

        ctx.moveTo(point[0], point[1]);

        for (i = 1; i < points.length; i += 1) {
            point = points[i];
            ctx.lineTo(point[0], point[1]);
        }

        return this;
    };

    /**
     * Trace a polygon.
     *
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.tracePolygon = function (points) {
        var ctx = this._ctx,
            i,
            point = points[0];

        ctx.moveTo(point[0], point[1]);

        for (i = 1; i < points.length; i += 1) {
            point = points[i];
            ctx.lineTo(point[0], point[1]);
        }

        ctx.closePath();

        return this;
    };

    /**
     * Trace a quadratic curve.
     *
     * @param {Number} x0
     * @param {Number} y0
     * @param {Number} cpx
     * @param {Number} cpy
     * @param {Number} x1
     * @param {Number} y1
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceQuadratic = function (x0, y0, cpx, cpy, x1, y1) {
        var ctx = this._ctx;

        ctx.moveTo(x0, y0);
        ctx.quadraticCurveTo(cpx, cpy, x1, y1);
        return this;
    };

    /**
     * Trace open quadratic curve using points.
     *
     * @param {Array} points must have a length of at least four points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceQuadraticCurve = function (points) {
        var ctx = this._ctx,
            point,
            i,
            nextPoint,
            ctrlPointX,
            ctrlPointY;

        if (points.length < 3) {
            return this;
        }

        //move to the first point
        point = points[0];
        ctx.moveTo(point[0], point[1]);

        //curve through the rest, stopping at each midpoint
        for (i = 1; i < points.length - 2; i += 1) {
            point = points[i];
            nextPoint = points[i + 1];

            ctrlPointX = (point[0] + nextPoint[0]) * 0.5;
            ctrlPointY = (point[1] + nextPoint[1]) * 0.5;
            ctx.quadraticCurveTo(point[0], point[1], ctrlPointX, ctrlPointY);
        }

        //curve through the last two points
        point = points[i];
        nextPoint = points[i + 1];
        ctx.quadraticCurveTo(point[0], point[1], nextPoint[0], nextPoint[1]);

        return this;
    };

    /**
     * Trace a closed quadratic curve path using points.
     *
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceClosedQuadraticCurve = function (points) {
        var ctx = this._ctx,
            ctrlPoint1,
            ctrlPoint,
            i,
            numPoints,
            point;

        if (points.length < 3) {
            return this;
        }

        ctrlPoint1 = qd.Point2D.create(0, 0);
        ctrlPoint = qd.Point2D.create(0, 0);
        numPoints = points.length;

        //find the first midpoint and move to it
        ctrlPoint1[0] = (points[0][0] + points[numPoints - 1][0]) / 2;
        ctrlPoint1[1] = (points[0][1] + points[numPoints - 1][1]) / 2;
        ctx.moveTo(ctrlPoint1[0], ctrlPoint1[1]);

        //curve through the rest, stopping at each midpoint
        for (i = 0; i < numPoints - 1; i += 1) {
            point = points[i];
            ctrlPoint[0] = (point[0] + points[i + 1][0]) / 2;
            ctrlPoint[1] = (point[1] + points[i + 1][1]) / 2;
            ctx.quadraticCurveTo(point[0], point[1], ctrlPoint[0], ctrlPoint[1]);
        }
        //curve through the last point, back to the first midpoint
        point = points[i];
        ctx.quadraticCurveTo(point[0], point[1], ctrlPoint1[0], ctrlPoint1[1]);

        return this;
    };

    /**
     * Trace a bezier curve.
     *
     * @param {Number} x0
     * @param {Number} y0
     * @param {Number} cp1x
     * @param {Number} cp1y
     * @param {Number} cp2x
     * @param {Number} cp2y
     * @param {Number} x1
     * @param {Number} y1
     * @return {qd.Canvas}
     */
    qd.Canvas.traceBezier = function (x0, y0, cp1x, cp1y, cp2x, cp2y, x1, y1) {
        var ctx = this._ctx;

        ctx.moveTo(x0, y0);
        ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x1, y1);
        return this;
    };

    /**
     * Trace bezier curve using points.
     *
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceBezierCurve = function (points) {
        var ctx = this._ctx;

        // TODO

        //ctx.moveTo(x0, y0);
        //ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x1, y1);
        return this;
    };

    /**
     * Trace a closed bezier curve using points.
     * @param {Array} points
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceClosedBezierCurve = function (points) {
        // TODO
    };

    /**
     * Not thread safe.
     *
     * @param origin
     * @param direction
     * @param styles
     * @return {*}
     */
    qd.Canvas.prototype.traceArrow = function (origin, direction, styles) {
        var CACHE = qd.Canvas.prototype.traceArrow.CACHE,

            arrowWidth = (styles) ? styles.arrowWidth : CACHE.ARROW_WIDTH,
            arrowHalfWidth = arrowWidth * 0.5,

            tip = qd.Point2D.add(CACHE.TIP, origin, direction),
            end = origin,
            base = qd.math.lerpPoint2DByDistance(CACHE.BASE, tip, end, arrowWidth),
            right = qd.math.perpendicularByDistance(CACHE.RIGHT, end, base, -arrowHalfWidth),
            left = qd.math.perpendicularByDistance(CACHE.LEFT, end, base, arrowHalfWidth),

            tipX = tip[0],
            tipY = tip[1];

        this.traceLine(end[0], end[1], tip[0], tip[1]);
        this.traceLine(tipX, tipY, right[0], right[1]);
        this.traceLine(tipX, tipY, left[0], left[1]);

        return this;
    };

    qd.Canvas.prototype.traceArrow.CACHE = {
        ARROW_WIDTH: qd.Canvas.STYLES.ARROW_WIDTH.DEFAULT,
        TIP: qd.Point2D.create(0, 0),
        BASE: qd.Point2D.create(0, 0),
        RIGHT: qd.Point2D.create(0, 0),
        LEFT: qd.Point2D.create(0, 0)
    };

    /**
     * Trace an infinite grid starting at any arbitrary point (x, y).
     *
     * @param {Number} cellSize
     * @param {Number} x
     * @param {Number} y
     * @return {qd.Canvas}
     */
    qd.Canvas.prototype.traceGrid = function (cellWidth, cellHeight, x, y) {
        var col,
            row,
            cols,
            rows,
            x0,
            y0,
            x1,
            y1,
            shiftX,
            shiftY,
            marginWidth,
            marginHeight,
            canvasWidth,
            canvasHeight,
            margins;

        margins = 1;

        shiftX = x % cellWidth;
        shiftY = y % cellHeight;

        marginWidth = margins * cellWidth;
        marginHeight = margins * cellHeight;

        canvasWidth = this.width();
        canvasHeight = this.height();

        cols = Math.ceil(canvasWidth / cellWidth);
        rows = Math.ceil(canvasHeight / cellHeight);

        x0 = -marginWidth - shiftX;
        x1 = canvasWidth + marginWidth - shiftX;

        for (row = -margins; row < rows + margins; row += 1)  {
            y0 = y1 = row * cellHeight - shiftY;

            this.traceLine(x0, y0, x1, y1);
        }

        y0 = -marginHeight - shiftY;
        y1 = canvasHeight + marginHeight - shiftY;

        for (col = -margins; col < cols + margins; col += 1)  {
            x0 = x1 = col * cellWidth  - shiftX;

            this.traceLine(x0, y0, x1, y1);
        }

        return this;
    };

    qd.Canvas.prototype.createLinearGradient = function (x1, y1, x2, y2) {
        // TODO: return qd.Gradient
        return this._ctx.createLinearGradient(x1, y1, x2, y2);
    };

    qd.Canvas.prototype.createRadialGradient = function (x1, y1, r1, x2, y2, r2) {
        // TODO: return qd.Gradient
        return this._ctx.createRadialGradient(x1, y1, r1, x2, y2, r2);
    };

    qd.Canvas.prototype.createPattern = function (image, type) {
        // TODO: return qd.Pattern
        return this._ctx.createPattern(image, type);
    };

    qd.Canvas.prototype.drawPath = function (styles) {
        this._styles.init(styles).apply(this._ctx);

        return this;
    };

    qd.Canvas.prototype.drawImage = function (image, x, y, width, height) {
        this._ctx.drawImage(image, x, y, width, height);
    };

    qd.Canvas.prototype.drawText = function (text, x, y, styles, maxWidth) {
        // TODO: Complete
        var ctx = this._ctx,
            stroke = false,
            fill = false,
            fontSize = null,
            fontFamily = null;

        ctx.save();

        qd.eachProperty(styles, function (style, value) {
            switch (style) {
                case "stroke":
                case "strokeColour":
                    if (value === "" || value === "transparent") {
                        stroke = false;
                    } else {
                        ctx.strokeStyle = value;
                        stroke = true;
                    }
                    break;
                case "fill":
                case "fillColour":
                    if (value === "" || value === "transparent") {
                        fill = false;
                    } else {
                        ctx.fillStyle = value;
                        fill = true;
                    }
                    break;
                case "fontSize":
                    fontSize = value;
                    break;
                case "fontFamily":
                    fontFamily = value;
                    break;

                default:
                    ctx[style] = value;
            }
        });

        ctx.font = qd.defaultValue(fontSize, qd.Canvas.STYLES.FONT_SIZE.DEFAULT)
            + " "
            + qd.defaultValue(fontFamily, qd.Canvas.STYLES.FONT_FAMILY.DEFAULT);

        if (fill) {
            ctx.fillText(text, x, y, maxWidth);
        }

        if (stroke) {
            ctx.strokeText(text, x, y, maxWidth);
        }

        ctx.restore();

        return this;
    };

    qd.Canvas.prototype.measureText = function (text) {
        return this._ctx.measureText(text);
    };

    qd.Canvas.prototype.toImage = function (mimeType) {
        return this._canvas.toDataURL(mimeType || "image/png");
    };

}(qd));
